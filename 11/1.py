from helpers import read_lines

def get_cols(image):
    cols = []
    for i in range(len(image)):
        cols.append([line[i] for line in image])
    return cols

def expand_image_rows(image):
    out_image = []
    for line in image:
        if not "#" in line:
            out_image.append(line)
            out_image.append(line)
        else:
            out_image.append(line)

def expand_image_cols(image):
    out_image = []
    for i, col in enumerate(get_cols(image)):
        if not "#" in col:
            out_image.append(line)
            out_image.append(line)
        else:
            out_image.append(line)
    return out_image


image = read_lines()
image = expand_image_rows(image)