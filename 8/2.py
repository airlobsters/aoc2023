import sys
from typing import Any
from pydantic import BaseModel
from copy import copy

from operator import mul
from functools import reduce # python3 compatibility
def product(l):
    return reduce(mul, l, 1)

class Instructions(BaseModel):
    instructions:str
    num_instructions:int=0
    index:int=0

    def model_post_init(self, _) -> None:
        self.num_instructions = len(self.instructions)
    
    def next(self):
        item = self.instructions[self.index]
        self.index = (self.index+1) % self.num_instructions
        return item

try:
    filename = sys.argv[1]
except:
    filename = "8/test_input3.txt"
with open(filename) as f:
    input = f.read().split("\n")
instructions = Instructions(instructions=input[0])
input = [a.split("=") for a in input[2:]]
graph = {a.strip():b.strip(" ()").split(", ") for a,b in input}

locations = [node for node in graph if node.endswith('A')]
steps = 0
def next_locations(locations, direction):
    for i in range(len(locations)):
        locations[i] = graph[locations[i]][direction]
    return locations
factors = [-1 for _ in locations]
while any((f == -1 for f in factors)):
    if instructions.next() == 'L':
        locations = next_locations(locations, 0)
        for i, l in enumerate(locations):
            if l.endswith('Z'):
                if factors[i] == -1:
                    factors[i] = steps + 1
    else:
        locations = next_locations(locations, 1)
        for i, l in enumerate(locations):
            if l.endswith('Z'):
                if factors[i] == -1:
                    factors[i] = steps + 1
    steps += 1
print(factors)
print(product(factors))
#there are multiple destinations, what does it mean?