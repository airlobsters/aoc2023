import sys
from typing import Any
from pydantic import BaseModel

class Instructions(BaseModel):
    instructions:str
    num_instructions:int=0
    index:int=0

    def model_post_init(self, _) -> None:
        self.num_instructions = len(self.instructions)
    
    def next(self):
        item = self.instructions[self.index]
        self.index = (self.index+1) % self.num_instructions
        return item

try:
    filename = sys.argv[1]
except:
    filename = "8/test_input.txt"
with open(filename) as f:
    input = f.read().split("\n")
instructions = Instructions(instructions=input[0])
input = [a.split("=") for a in input[2:]]
graph = {a.strip():b.strip(" ()").split(", ") for a,b in input}
print(instructions)
print(graph)

location = 'AAA'
steps = 0
while location != 'ZZZ':
    if instructions.next() == 'L':
        location = graph[location][0]
    else:
        location = graph[location][1]
    steps += 1

print(steps)