from pydantic import BaseModel
from enum import Enum, auto
from collections import Counter

HAND_LENGTH = 5
CARD_ORDER = list(reversed("A, K, Q, T, 9, 8, 7, 6, 5, 4, 3, 2, J".split(", ")))


class HandType(Enum):
    five_of_a_kind = 6
    four_of_a_kind = 5
    full_house = 4
    three_of_a_kind = 3
    two_pair = 2
    one_pair = 1
    high_card = 0

    def __lt__(self, other):
        return self.value < other.value


class Hand(BaseModel):
    cards: str
    bid: int
    type: HandType = None

    def model_post_init(self, _):
        self.type = self.compute_type()

    @staticmethod
    def from_str(input):
        cards, bid = input.split()
        return Hand(cards=cards, bid=int(bid))

    def compute_type(self):
        def is_full_house(counts, num_jokers):
            if num_jokers == 2:
                return counts[0][1] != 1  # there has to be at least two the same
            if num_jokers == 1:
                return  (counts[0][1] == 3 and counts[1][1] == 1) or \
                        (counts[0][1] == 2 and counts[1][1] == 2)
            return counts[0][1] == 3 and counts[1][1] == 2

        c = Counter(self.cards)
        num_jokers = c["J"] if "J" in c else 0
        del c["J"]
        counts = c.most_common()
        if num_jokers == 5 or counts[0][1] + num_jokers == 5:
            return HandType.five_of_a_kind
        if counts[0][1] + num_jokers == 4:
            return HandType.four_of_a_kind
        if is_full_house(counts, num_jokers):
            return HandType.full_house
        if counts[0][1] + num_jokers == 3:
            return HandType.three_of_a_kind
        if counts[0][1] == 2 and counts[1][1] + num_jokers == 2:
            return HandType.two_pair
        if counts[0][1] + num_jokers == 2:
            return HandType.one_pair
        return HandType.high_card

    def __lt__(self, other):
        def eq_card(l, r):
            return CARD_ORDER.index(l) == CARD_ORDER.index(r)

        def lt_card(l, r):
            return CARD_ORDER.index(l) < CARD_ORDER.index(r)

        if self.type == other.type:
            i = 0
            while eq_card(self.cards[i], other.cards[i]) and i < HAND_LENGTH:
                i += 1
            return lt_card(self.cards[i], other.cards[i])
        return self.type < other.type


import sys

try:
    filename = sys.argv[1]
except:
    filename = "7/test_input.txt"
with open(filename) as f:
    input = f.read()
hands = input.split("\n")
hands = [Hand.from_str(x) for x in hands]
hands = sorted(hands)
print(sum([x.bid * i for x, i in zip(hands, range(1, len(hands) + 1))]))
