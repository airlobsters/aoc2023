from helpers import read_lines
lines = read_lines()
histories = [list(map(int, l.split())) for l in lines]

def next(h):
    return [b-a for a,b in zip(h,h[1:])]

def get_sequences(sequences, current):
    if all(i == 0 for i in current):
        return sequences
    sequences.append(current)
    return get_sequences(sequences, next(current))

def extend_sequences(sequences):
    sequences.reverse()
    d = 0
    for s in sequences:
        d = s[0] - d 
        yield d

total = 0
for h in histories:
    sequences = get_sequences([], h)
    total += list(extend_sequences(sequences))[-1]
print(total)