from enum import Enum, auto
from pydantic import BaseModel
from helpers import read_lines


class Position(BaseModel):
    x: int
    y: int

    def __eq__(self, other: object) -> bool:
        return self.x == other.x and self.y == other.y


class Direction(Enum):
    NORTH = auto()
    SOUTH = auto()
    EAST = auto()
    WEST = auto()


pipes = {
    "|": {  # is a vertical pipe connecting north and south.
        Direction.NORTH: Direction.NORTH,
        Direction.SOUTH: Direction.SOUTH,
    },
    "-": {  # is a horizontal pipe connecting east and west.
        Direction.EAST: Direction.EAST,
        Direction.WEST: Direction.WEST,
    },
    "L": {  # is a 90-degree bend connecting north and east.
        Direction.SOUTH: Direction.EAST,
        Direction.WEST: Direction.NORTH,
    },
    "J": {  # is a 90-degree bend connecting north and west.
        Direction.EAST: Direction.NORTH,
        Direction.SOUTH: Direction.WEST,
    },
    "7": {  # is a 90-degree bend connecting south and west.
        Direction.NORTH: Direction.WEST,
        Direction.EAST: Direction.SOUTH,
    },
    "F": {  # is a 90-degree bend connecting south and east.
        Direction.NORTH: Direction.EAST,
        Direction.WEST: Direction.SOUTH,
    },
    "S": {d: d for d in Direction},
}


def pipe_at(p: Position):
    return lines[p.y][p.x]


def move(p, d):
    x, y = (p.x, p.y)
    if d == Direction.NORTH:
        y -= 1
    if d == Direction.SOUTH:
        y += 1
    if d == Direction.EAST:
        x += 1
    if d == Direction.WEST:
        x -= 1
    new_p = Position(x=x, y=y)
    new_d = pipes[pipe_at(new_p)][d]
    return new_p, new_d


def initial_position():
    for i, line in enumerate(lines):
        x = line.find("S")
        if x != -1:
            return Position(x=x, y=i)


def initial_direction(p):
    for d in Direction:
        try:
            if d in pipes[pipe_at(move(p, d)[0])]:
                return d
        except KeyError:
            pass


lines = read_lines()
pi = initial_position()
d = initial_direction(pi)
p, d = move(pi, d)
moves = 1
while p != pi:
    moves += 1
    p, d = move(p, d)
print(moves//2)