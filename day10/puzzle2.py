from day10.puzzle1 import *

visited = [[False for _ in range(len(line))]for line in lines]
pi = initial_position()
visited[pi.y][pi.x] = True
d = initial_direction(pi)
p, d = move(pi, d)
moves = 1
while p != pi:
    visited[p.y][p.x] = True
    moves += 1
    p, d = move(p, d)
print(p,d)
enclosed = {False: 0, True: 0}
for y, line in enumerate(lines):
    cur_enclosed = False
    final_pipe_pos = len(line) - 1
    while line[final_pipe_pos] == "." and final_pipe_pos >= 1:
        final_pipe_pos -= 1
    for x, c in enumerate(line):
        if not visited[y][x]:
            c = "."
        if x >= final_pipe_pos:
            continue
        if c == ".":
            enclosed[cur_enclosed] += 1
            print(cur_enclosed, (y,x), lines[y][x])
        else:
            cur_enclosed = not cur_enclosed
print(enclosed)
