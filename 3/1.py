from pydantic import BaseModel
from typing import List
from enum import Enum, auto
import sys

class State(Enum):
    looking_for_number = auto()
    validating_number = auto()
    parsing_number = auto()
    finished = auto()


class Parser(BaseModel):
    state: State = State.looking_for_number
    current_row: int = 0
    current_col: int = 0
    current_number: str = ""
    age: int = 0
    schematic: List[str] = []
    width: int = 0
    height: int = 0
    sum: int = 0

    def next_pos(self):
        if self.current_col < self.width - 1:
            self.current_col += 1
            return
        if self.current_row < self.height - 1:
            if self.state == State.parsing_number:
                self.state = State.validating_number
                return
            self.current_row += 1
            self.current_col = 0
            return
        self.state = State.finished

    def current_char(self):
        cur_char = self.schematic[self.current_row][self.current_col]
        print(cur_char, self.current_row, self.current_col, self.state, "".join(self.current_number_adjacent()))
        return cur_char

    def get_row(self, row):
        start = max(0, self.current_col - 1 - len(self.current_number))
        end = min(self.current_col + 1, self.width)
        return self.schematic[row][start:end]

    def current_number_adjacent(self):
        adjacent = []
        # append row above
        if self.current_row - 1 >= 0:
            adjacent += self.get_row(self.current_row - 1)
        # append current row
        adjacent += self.get_row(self.current_row)
        # append row below
        if self.current_row + 1 < self.height:
            adjacent += self.get_row(self.current_row + 1)
        return adjacent

    def validate_number(self):
        if any(
            not a.isnumeric() and not a == "." for a in self.current_number_adjacent()
        ):
            return True

    def tick(self):
        match self.state:
            case State.looking_for_number:
                if self.current_char().isnumeric():
                    self.state = State.parsing_number
                else:
                    self.next_pos()
            case State.parsing_number:
                if self.current_char().isnumeric():
                    self.current_number += self.current_char()
                    self.next_pos()
                else:
                    self.state = State.validating_number
            case State.validating_number:
                if self.validate_number():
                    self.sum += int(self.current_number)
                    print(f"adding {self.current_number}")
                else:
                    print(f"ignoring {self.current_number}")
                self.current_number = ""
                self.next_pos()
                self.state = State.looking_for_number
            case _:
                raise Exception()

    def parse(self, schematic):
        self.schematic = schematic
        self.width = len(schematic[0])
        self.height = len(schematic)

        while self.state != State.finished and self.age < self.width * self.height * 2:
            self.age += 1
            self.tick()
        return self.sum

try:
    filename = sys.argv[1]
except:
    filename = "3/input.txt"
with open(filename) as f:
    input = f.read()
schematic = input.split("\n")
p = Parser()
print(p.parse(schematic))
