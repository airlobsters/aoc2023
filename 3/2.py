import sys
import re

def get_row(row, current_col):
    start = max(0, current_col - 1)
    while schematic[row][start].isnumeric() and start > 0:
        start -= 1
    end = min(current_col + 1, WIDTH)
    while schematic[row][end].isnumeric() and end < WIDTH -1:
        end += 1
    numbers = "".join(schematic[row][start:end+1])
    numbers = re.split(r'\.+|\*', numbers)
    return [int(n) for n in numbers if n]

def get_adjacent_numbers(row, col):
    adjacent = []
    # append row above
    if row - 1 >= 0:
        adjacent += get_row(row - 1, col)
    # append current row
    adjacent += get_row(row, col)
    # append bottom row 
    if row + 1 < HEIGHT:
        adjacent += get_row(row + 1, col)
    
    return adjacent

try:
    filename = sys.argv[1]
except:
    filename = "3/test_input.txt"
with open(filename) as f:
    input = f.read()

schematic = input.split("\n")
HEIGHT = len(schematic)
WIDTH = len(schematic[0])
gear_sum = 0
for i, row in enumerate(schematic):
    star_is = [i for i,x in enumerate(row) if x == "*"]
    for star_i in star_is:
        adjacent_numbers = get_adjacent_numbers(i, star_i)
        if len(adjacent_numbers) == 2:
            print(f"found star: {i,star_i, adjacent_numbers}")
            gear_sum += adjacent_numbers[0] * adjacent_numbers[1]
print(gear_sum)