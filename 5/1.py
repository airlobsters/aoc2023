import sys
try:
    filename = sys.argv[1]
except:
    filename = "5/test_input.txt"
with open(filename) as f:
    input = f.read()
lines = [l for l in input.split("\n") if len(l)]
seeds = [int(x) for x in lines[0].split(":")[1].split()]
maps = {}
for line in lines[1:]:
    if not line[0].isnumeric():
        name = line.replace(" map:","")
        maps[name] = []
    else:
        destination, source, length = [int(x) for x in line.split()]
        maps[name].append((destination, source, length))

def map_seed(seed):
    for name, mappings in maps.items():
        for destination, source, length in mappings:
            if seed in range(source,source+length):
                seed = destination + (seed-source)
                break
            # print(f"mapping {name}, {seed}")
    return seed 
print(min([map_seed(seed) for seed in seeds]))