import sys
from copy import copy
try:
    filename = sys.argv[1]
except:
    filename = "5/test_input.txt"
with open(filename) as f:
    input = f.read()
lines = [l for l in input.split("\n") if len(l)]
seed_numbers = [int(x) for x in lines[0].split(":")[1].split()]
seeds = []
for start, length in zip(seed_numbers[::2], seed_numbers[1::2]):
    seeds.append(range(start,start+length))
maps = {}
for line in lines[1:]:
    if not line[0].isnumeric():
        name = line.replace(" map:","")
        maps[name] = []
    else:
        destination, source, length = [int(x) for x in line.split()]
        maps[name].append((range(source,source+length), destination-source))

def map_seed(seed, mapping):
    mapped_seeds = []
    unmapped_seeds = []
    map_range, d = mapping
    #the part of the range below the mapping
    if seed.start < map_range.start:
        unmapped_seeds.append(range(seed.start, min(seed.stop, map_range.start-1)))
    #the part of the range within the mapping
    if max(seed.start,map_range.start) < min(seed.stop, map_range.stop):
        inner_part = range(max(seed.start,map_range.start)+d,min(seed.stop, map_range.stop)+d)
        mapped_seeds.append(inner_part)
    #the part of the range above the mapping
    if seed.stop > map_range.stop:
        unmapped_seeds.append(range(max(seed.start,map_range.stop+1), seed.stop))
    print(f"{seed, mapping, unmapped_seeds + mapped_seeds}")
    return unmapped_seeds, mapped_seeds

# too complex, change this to have either one seed, or one mapping
def map_seeds(input_seeds, input_mappings):
    mapped_seeds = []
    final_unmapped_seeds = []
    for seed in input_seeds:
        mappings = copy(input_mappings)
        if len(mappings) == 0:
            return unmapped_seeds, []
        else:
            cur_mapping = mappings.pop()
            unmapped_seeds, mapped_seeds = map_seed(seed, cur_mapping)
            for seed in unmapped_seeds:
                recurse_unmapped_seeds, recurse_mapped_seeds = map_seeds([seed], mappings)
                mapped_seeds += recurse_mapped_seeds
                final_unmapped_seeds += recurse_unmapped_seeds
    return final_unmapped_seeds, mapped_seeds

# print(map_seed(range(1,100), (range(10,20),190)))
# print(map_seed(range(1,12), (range(10,20),190)))
# print(map_seed(range(19,100), (range(10,20),190)))
# print(map_seeds([range(1,100)], [(range(10,20),190)]))
for name, mappings in maps.items():
    print(f"mapping {name}")
    unmapped_seeds, mapped_seeds = map_seeds(seeds, mappings)
    seeds = list(set(unmapped_seeds + mapped_seeds))
    print(seeds)
    break
# for s in seeds:
#     print(s)