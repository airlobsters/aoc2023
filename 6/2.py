import sys
try:
    filename = sys.argv[1]
except:
    filename = "6/test_input.txt"
with open(filename) as f:
    input = f.read()
lines = [l for l in input.split("\n") if len(l)]
time = int("".join(lines[0].split(":")[1].split()))
distance = int("".join(lines[1].split(":")[1].split()))
races = [(time, distance)]
print(races)


def get_times(race_duration):
    return [(i,race_duration-i) for i in range(race_duration)]

def get_distance(button_time, race_time):
    return button_time * race_time
product = 1
for total_time, distance_record in races:
    distances = [get_distance(*time) > distance_record for time in get_times(total_time)]
    product *= sum(distances)
print(product)