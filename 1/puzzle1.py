def firstnumber(s):
    for i in s:
        if i.isdigit():
            return i

def lastnumber(s):
    for i in reversed(s):
        if i.isdigit():
            return i

def solve(input):
    firstandlast = [(firstnumber(i), lastnumber(i)) for i in input]
    numbers = [int("".join(i)) for i in firstandlast]
    print(sum(numbers))

if __name__ == "__main__":
    puzzle_input = open("input.txt").read().split("\n")
    puzzle_input = puzzle_input[:-1]
    solve(puzzle_input)