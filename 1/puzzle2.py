written_numbers = "zero one two three four five six seven eight nine".split()
mapping = {written_numbers[i]: i for i in range(10)}

def first(s):
    positions = {}
    for i, number_str in enumerate(written_numbers):
        str_pos = s.find(number_str)
        num_pos = s.find(str(i))
        found = [i for i in [str_pos,num_pos] if i != -1]
        if len(found) > 0:
            positions[str(i)] = min(found)
    positions = sorted(positions.items(), key=lambda item: item[1])
    return positions[0][0] 

def last(s):
    positions = {}
    for i, number_str in enumerate(written_numbers):
        str_pos = s.rfind(number_str)
        num_pos = s.rfind(str(i))
        found = [i for i in [str_pos,num_pos] if i != -1]
        if len(found) > 0:
            positions[str(i)] = max(found)
    positions = sorted(positions.items(), key=lambda item: item[1])
    return positions[-1][0] 

def solve(input):
    firstandlast = [(first(i), last(i)) for i in input]
    numbers = [int("".join(i)) for i in firstandlast]
    print(sum(numbers))

if __name__ == "__main__":
    puzzle_input = open("input.txt").read().split("\n")
    solve(puzzle_input)