from puzzle2 import first, last

def test_first_written():
    assert first("eightwothree") == "8"
    assert first("eight8zlctbmsixhrvbpjb84nnmlcqkzrsix") == "8"
    
def test_last():
    assert last("eight8zlctbmsixhrvbpjb84nnmlcqkzrsix") == "6"
