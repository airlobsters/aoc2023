#!/bin/bash

# Check if the number of arguments is correct
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <folder_name>"
    exit 1
fi

# Get the folder name from the command line argument
folder_name=$1

# Create the folder
mkdir "$folder_name"

# Create files inside the folder
touch "$folder_name/1.py"
touch "$folder_name/2.py"
touch "$folder_name/test_input.txt"

echo "Folder '$folder_name' created with files '1.py', '2.py', and 'test_input.txt'."
