
import sys
from pydantic import BaseModel
from typing import Set

class Card(BaseModel):
    id:int
    winners:Set[int]
    numbers:Set[int]

    @staticmethod
    def from_str(line):
        id, rest = line.split(":")
        id = int(id.replace("Card ",""))
        winners, numbers = rest.split("|")
        winners = {int(x) for x in winners.split()}
        numbers = {int(x) for x in numbers.split()}
        return Card(id=id, winners=winners, numbers=numbers)

    def winning(self):
        return self.winners.intersection(self.numbers)
    
    def score(self):
        if self.winning():
            return 1 * 2**(len(self.winning())-1)
        else:
            return 0
    
try:
    filename = sys.argv[1]
except:
    filename = "4/test_input.txt"
with open(filename) as f:
    input = f.read()
cards = input.split("\n")
cards = [Card.from_str(x) for x in cards]
print(sum([c.score() for c in cards]))