
import sys
from pydantic import BaseModel
from typing import Set

class Card(BaseModel):
    id:int
    num_winners:int=0
    winners:Set[int]
    numbers:Set[int]
    copies:int=1

    @staticmethod
    def from_str(line):
        id, rest = line.split(":")
        id = int(id.replace("Card ",""))
        winners, numbers = rest.split("|")
        winners = {int(x) for x in winners.split()}
        numbers = {int(x) for x in numbers.split()}
        return Card(id=id, winners=winners, numbers=numbers)

    def winning(self):
        return self.winners.intersection(self.numbers)
    
try:
    filename = sys.argv[1]
except:
    filename = "4/test_input.txt"
with open(filename) as f:
    input = f.read()
cards = input.split("\n")
cards = [Card.from_str(x) for x in cards]
for c in cards:
    c.num_winners = len(c.winning())
for i, card in enumerate(cards):
    for j in range(card.num_winners):
        cards[i+j+1].copies += card.copies
print(sum([c.copies for c in cards]))