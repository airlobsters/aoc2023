from typing import List, Optional
from pydantic import BaseModel


class Move(BaseModel):
    red: Optional[int] = 0
    green: Optional[int] = 0
    blue: Optional[int] = 0

    @staticmethod
    def from_str(line):
        counts = {}
        for count in line.split(","):
            amount, color = count.strip().split(" ")
            counts[color] = amount
        return Move.model_validate(counts)

    def possible(self):
        return self.red <= RED and \
            self.green <= GREEN and \
            self.blue <= BLUE

class Game(BaseModel):
    id: int
    moves: List[Move]

    @staticmethod
    def from_string(line):
        id, moves = line.split(":")
        id = int(id.replace("Game ", ""))
        moves = [Move.from_str(move) for move in moves.split(";")]
        return Game(id=id, moves=moves)

    def possible(self):
        return all(m.possible() for m in self.moves)



RED, GREEN, BLUE = (12, 13, 14)


def parse_games(filename):
    lines = open(filename).read().split("\n")
    return [Game.from_string(line) for line in lines]
games = parse_games("input.txt")
print(sum([g.id for g in games if g.possible()]))