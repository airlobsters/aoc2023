from operator import mul
from functools import reduce # python3 compatibility
def product(l):
    return reduce(mul, l, 1)

from typing import List, Optional
from pydantic import BaseModel
from copy import copy

class Move(BaseModel):
    red: Optional[int] = 0
    green: Optional[int] = 0
    blue: Optional[int] = 0

    @staticmethod
    def from_str(line):
        counts = {}
        for count in line.split(","):
            amount, color = count.strip().split(" ")
            counts[color] = amount
        return Move.model_validate(counts)

class Game(BaseModel):
    id: int
    moves: List[Move]

    @staticmethod
    def from_string(line):
        id, moves = line.split(":")
        id = int(id.replace("Game ", ""))
        moves = [Move.from_str(move) for move in moves.split(";")]
        return Game(id=id, moves=moves)

    def fewest(self):
        minimum = copy(self.moves[0])
        for m in self.moves[1:]:
            if m.red > minimum.red:
                minimum.red = m.red
            if m.green > minimum.green:
                minimum.green = m.green
            if m.blue > minimum.blue:
                minimum.blue = m.blue
        return minimum
    
    def power(self):
        return product(self.fewest().model_dump().values())


RED, GREEN, BLUE = (12, 13, 14)


def parse_games(filename):
    lines = open(filename).read().split("\n")
    return [Game.from_string(line) for line in lines]
games = parse_games("input.txt")
print(sum([g.power() for g in games]))