import sys
def read_lines():
    try:
        filename = sys.argv[1]
    except:
        day = sys.argv[0].split('/')[-2]
        filename = f"{day}/test_input.txt"
    with open(filename) as f:
        return f.read().split("\n")